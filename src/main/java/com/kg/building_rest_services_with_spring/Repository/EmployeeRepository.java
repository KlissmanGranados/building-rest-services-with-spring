package com.kg.building_rest_services_with_spring.Repository;

import com.kg.building_rest_services_with_spring.Model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee,Long> {
}
