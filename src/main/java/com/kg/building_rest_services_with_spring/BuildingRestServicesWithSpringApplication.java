package com.kg.building_rest_services_with_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuildingRestServicesWithSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(BuildingRestServicesWithSpringApplication.class, args);
    }

}
