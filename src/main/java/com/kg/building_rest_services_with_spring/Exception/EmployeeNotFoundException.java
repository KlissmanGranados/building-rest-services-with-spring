package com.kg.building_rest_services_with_spring.Exception;

public class EmployeeNotFoundException extends RuntimeException{
    public EmployeeNotFoundException(Long id){
        super("Could not find employee " + id);
    }
}
